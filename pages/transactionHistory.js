import React, { useState, useEffect } from 'react';
import { Button, Row, Col, Card, Form } from 'react-bootstrap';
import moment from 'moment';
import Router from 'next/router';

import View from '../components/View';
import ShowRecords from '../components/ShowRecords';
// import Filter from '../../components/Filter';

export default function index(){

    return (
        <View title = {'Budget Tracker: Records History'}>
            <Row className="justify-content-center">
                <Col className="text-center">
                    <Search />
                </Col>
            </Row>
        </View>
    )
}


const Search = () => {

    const [records, setRecords] = useState([])
    const [search, setSearch] = useState('')
    const [category, SetCategory] = useState('')

    useEffect(() => {

        const options = {
        headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
        };
        
        fetch(`https://rocky-brook-64027.herokuapp.com/api/users/details`, options)
        .then(res => res.json())
        .then(data => {

            const records = data.records

            setRecords(records)
        })
    }, [])

    useEffect(() => {

    }, [category])



    const filteredName = search.length === 0 ? records : records.filter(record => record.categoryName.toLowerCase().includes(search.toLowerCase()))
        // console.log(filteredRecords)

    const addTransaction = () => {
        Router.push('/addRecord')
    }

    return (
        <React.Fragment>
            <Card className="p-3 mb-3">
                <Form onSubmit={ e => addCategory(e) }>
                    <Form.Row className="text-center d-flex justify-content-center">
                        {/*For search input*/}
                        <Col sm="6" md="3">
                            <Form.Group controlId="searchName">
                                <Form.Control 
                                    type="text"
                                    placeholder="Search name"
                                    value={search}
                                    onChange={(e) => setSearch(e.target.value)}
                                    
                                />
                            </Form.Group>
                        </Col>
                        {/*For filter select*/}
                        <Col sm="6" md="3">
                            <Form.Control 
                                onChange={(e) => SetCategory(e.target.value)}
                                as="select"
                                >
                                <option value="">All</option>
                                <option value="Income">Income</option>
                                <option value="Expense">Expense</option>
                            </Form.Control>
                        </Col>
                    </Form.Row>
                </Form>
                <Button className="mt-3 bg-primary w-50 mx-auto" onClick={addTransaction}>
                    Add Record
                </Button>
            </Card>
            <ShowRecords records={filteredName} category={category}/>
        </React.Fragment>
    )
}