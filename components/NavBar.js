import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from 'react-bootstrap';
import Link from 'next/link';
import React, { useContext } from 'react';
import UserContext from '../UserContext';



export default function NavBar() {
    
    const { user } = useContext(UserContext);



    return(

        <Navbar bg="light" expand="lg" className="text-white bg-dark">
            <Navbar.Brand href="/profile" className="text-primary">Budget Tracker</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">

                        {(user.id !== null)
                            ?
                        
                            <React.Fragment >
                                <Nav.Link href="/profile" className="text-white">Profile</Nav.Link>
                                <Nav.Link href="/addCategory" className="text-white">Add Category Name</Nav.Link>
                                <Nav.Link href="/showCategory" className="text-white">Show Categories</Nav.Link>
                                <Nav.Link href="/addRecord" className="text-white">Add Record</Nav.Link>
                                <Nav.Link href="/transactionHistory" className="text-white">Transaction History</Nav.Link>
                                      <NavDropdown className="bg-light rounded" title="Analytics" id="basic-nav-dropdown">
                                            <NavDropdown.Item href="/breakdown">Breakdown</NavDropdown.Item>
                                            <NavDropdown.Item href="/trends">Trends</NavDropdown.Item>
                                            <NavDropdown.Divider />
                                      </NavDropdown>
                                <Nav.Link href="/logout" className="text-white">Logout</Nav.Link>
                                </React.Fragment>  
                                 :
                                <React.Fragment>  
                                <Nav.Link href="/login" className="text-white">Login</Nav.Link>
                                <Nav.Link href="/register" className="text-white">Register</Nav.Link>
                                </React.Fragment>
                     } 
                    </Nav>
        </Navbar.Collapse>
        </Navbar>
     )
}

