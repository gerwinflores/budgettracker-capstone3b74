import React from 'react';
import { Form, Button, Col, Row } from 'react-bootstrap';

export default function addExpenseTransaction() {

	const [transactionName, setTransactionName] = useState('');
	const [category, setCategory] = useState ('');
	const [description, setDescription] = useState ('');
	const [amount, setAmount] = useState('');

	function addExpenseTransaction(e){

   		 e.preventDefault();

   		 fetch(`https://rocky-brook-64027.herokuapp.com/api/users/add-expense-transaction`, {
				method: 'POST',
				headers: {
	                Authorization: `Bearer ${localStorage.getItem('token')}`,
	                'Content-Type': 'application/json'
	            },
	            body: JSON.stringify({
	                transactionName: transactionName,
		        	type: "expense",
		        	category: category,
		        	description: description,
		        	amount: amount
	            })
	     })
	     .then(res => res.json())
	     .then(data =>{

	        console.log(data)


	     })

   	 }

   	 useEffect(() => {
		

	}, [ transactionName, category, description, amount ])


	return (
		<Form onSubmit={e => addIncomeCategory(e)}>
		  <Form.Group>
		    <Form.Label>Transaction Name</Form.Label>
		    <Form.Control
		    	type="text"
				value={transactionName}
				onChange={e => setTransactionName(e.target.value)}
				required 
		    />
		    <Form.Label>Transaction Name</Form.Label>
		    <Form.Control
		    	type="text"
				value={transactionName}
				onChange={e => setTransactionName(e.target.value)}
				required 
		    />
		  </Form.Group>
		</Form>	
	)
}